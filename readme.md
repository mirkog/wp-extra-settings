WP-Extra-Settings
=================

WP Extra Settings gives you control over many different options that are manageable without accessing the code otherwise. This plugin will thus enable you to control almost every aspect of WordPress making a more professional tool out of it.

Installation
------------

You can install this plugin either through the plugin repository browser embedded in WordPress or loading it via FTP in the `/wp-content/plugins/` folder.
The plugin can be activated only through the plugin list in the Dashboard.